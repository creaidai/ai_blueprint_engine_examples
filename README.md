# AI Blueprint Engine Examples

This repository holds various example were the [AI Blueprint Engine](https://blueprints.creaidai.com) was used to generate code for various models of different sizes.


### Logistic Regression

The code generated after building a logistic regression model that integrates multiple input sources in our Web UI.
[Show in Web UI](https://blueprints.creaidai.com/examples/logistic_regression)


### Classic CNN

The code generated after building a classic CNN arrchitecture in our Web UI.
[Show in Web UI](https://blueprints.creaidai.com/examples/classic_cnn)


### LSTM Signal Classification

The code generated after building a basic classificator for fixed-length signal classification in our Web UI.
[Show in Web UI](https://blueprints.creaidai.com/examples/lstm_signal_classification)


### Inception

The code generated after reproducing the architecture of Google's Inception model for image classification in our Web UI.
[Show in Web UI](https://blueprints.creaidai.com/examples/inception)

Christian Szegedy, Wei Liu, Yangqing Jia, Pierre Sermanet, Scott Reed, Dragomir Anguelov, Dumitru Erhan, Vincent Vanhoucke, Andrew Rabinovich.
Going Deeper with Convolutions.
Computer Vision and Pattern Recognition (CVPR), 2015.
[Paper](https://arxiv.org/pdf/1409.4842.pdf)


### VGG19

The code generated after reproducing the architecture of the popular image classification model in out Web UI.
[Show in Web UI](https://blueprints.creaidai.com/examples/vgg19)

Karen Simonyan, Andrew Zisserman.
Very Deep Convolutional Networks for Large-Scale Image Recognition.
arXiv:1409.1556, 2015
[Paper](https://arxiv.org/pdf/1409.1556.pdf)


### Titanic ML from Disaster

The example application used in our blog (coming soon) where we introduced the AI Blueprint Engine.
[Show in Web UI](https://blueprints.creaidai.com/examples/titanic_model)


# Company Website

[creaidAI](https://creaidai.com/) - Bridging the gap between ease of use and expressive power in artificial intelligence development.

# inception

## Project overview

Your project description.

## Setup

### Dependencies

* h5py
* numpy
* pandas
* tensorflow==1.5

### Project Setup

For running the code and/or continuing development, you need to add the project to your ``PYTHONPATH`` in order for Python to find module imports inside the project directory.

#### Linux / MacOS
We assume in the following that the project is located at ``/home/user/projects``. Please substitute this path by the actual path of your project directory.
The project path can be assigned to ``PYTHONPATH`` for the duration of executing the subsequent command.

```bash
$ PYTHONPATH=/home/user/projects/inception python training.py
```

We recommend to set the ``PYTHONPATH`` permanently. In case you run Python from within an editor, you can typically set the ``PYTHONPATH`` in the editor configuration. Please consult the documentation of your editor. If you are executing Python from the shell, open the ``$HOME/.bashrc`` file with your favorite editor and add the following line at the bottom of the file:

```bash
$ export PYTHONPATH=$PYTHONPATH:/home/user/projects/inception
```
To make the changes effective, open a new shell or run the following command in the current shell:

```bash
$ source $HOME/.bashrc
```

#### Windows
On Windows 7, environment variables can be set at ``My Computer > Properties > Advanced System Settings > Environment Variables``. Search under ``System variables`` for ``PYTHONPATH``. If this entry exists, click ``Edit...`` and append the path of the project. If this entry does not exist, click ``New...``, set ``Variable name`` to ``PYTHONPATH`` and ``Variable value`` to the path of the project.

On Windows 10, the environment variables can be se at ``Start > Settings > System > About > System info > Advanced system settings > Environment variables``. Search under ``System variables`` for ``PYTHONPATH``. If this entry exists, click ``Edit...`` and append the path of the project. If this entry does not exist, click ``New...``, set ``Variable name`` to ``PYTHONPATH`` and ``Variable value`` to the path of the project.

If you run Python from within an editor, please consult its documentation to find out how to set the ``PYTHONPATH`` environment variable.

#### Docker

Docker images to run the generated code on both CPU and GPU can be built using the provided Dockerfiles. In order to build a Docker image with CPU-only support, execute the following command:

```bash
$ docker build -t inception:latest .
```

Similarly, a Docker image with GPU support can be built by executing the following command:

```bash
$ docker build -t inception:latest -f Dockerfile.gpu .
```

In order to run, e.g., the training script inside a Docker container, data files need to be made available inside the container using [Docker volumes](https://docs.docker.com/storage/volumes/#choose-the--v-or---mount-flag). Assuming `/path/to/your/data` is the directory where training data is stored, `/path/inside/container` is the path where this directory is mounted inside the container, and there is the name of the input source variable is `source_file_1`, then the training script can be executed inside a Docker container as follows:

```bash
$ docker run --rm -v /path/to/data:/path/in/container \
        inception:latest \
            python inception/training.py \
                --source_file_1 /path/inside/container/data.csv
```

## Generated Code Overview
In the following a description of the generated code and its basic content is provided. For some of the documentation the Markdown [mermaid](https://mermaidjs.github.io) extension is required to visualize graphs. Alternatively, you can use the [mermaid live editor](https://mermaidjs.github.io/mermaid-live-editor).

### Blueprint

#### Model summary
* A 64 layer neural network that predicts 3 target sources from 1 input source.

#### Model inputs
* `images` : RGB Image (n x 224 x 224 x 3)

#### Model targets
* `aux_target1` : Sparse Multi Class (n)
* `aux_target2` : Sparse Multi Class (n)
* `classes` : Sparse Multi Class (n)

#### Data Preprocessing
```mermaid
graph BT;
    InputData/images-->|n x 224 x 224 x 3|NNInput/input_;
    style NNInput/input_ fill:#d5dbdb, stroke:#626262;
    TargetData/aux_target1-->|n|NNTarget/aux_loss1;
    style NNTarget/aux_loss1 fill:#abb2b9, stroke:#626262;
    TargetData/aux_target2-->|n|NNTarget/aux_loss2;
    style NNTarget/aux_loss2 fill:#abb2b9, stroke:#626262;
    TargetData/classes-->|n|NNTarget/main_loss;
    style NNTarget/main_loss fill:#abb2b9, stroke:#626262;
```

#### Model Architecture
```mermaid
graph BT;
    NNInput/input_-->|n x 224 x 224 x 3|2DConvolution/conv1;
    style 2DConvolution/conv1 fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1-->|n x 112 x 112 x 64|2DMaxPooling/maxp1;
    style 2DMaxPooling/maxp1 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp1-->|n x 56 x 56 x 64|2DConvolution/conv2;
    style 2DConvolution/conv2 fill:#a9cce3, stroke:#626262;
    2DConvolution/conv2-->|n x 56 x 56 x 64|2DConvolution/conv3;
    style 2DConvolution/conv3 fill:#a9cce3, stroke:#626262;
    2DConvolution/conv3-->|n x 56 x 56 x 192|2DMaxPooling/maxp2;
    style 2DMaxPooling/maxp2 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp2-->|n x 28 x 28 x 192|2DConvolution/conv1x1_3a;
    style 2DConvolution/conv1x1_3a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp2-->|n x 28 x 28 x 192|2DConvolution/reduce3x3_3a;
    style 2DConvolution/reduce3x3_3a fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_3a-->|n x 28 x 28 x 96|2DConvolution/conv3x3_3a;
    style 2DConvolution/conv3x3_3a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp2-->|n x 28 x 28 x 192|2DConvolution/reduce5x5_3a;
    style 2DConvolution/reduce5x5_3a fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_3a-->|n x 28 x 28 x 96|2DConvolution/conv5x5_3a;
    style 2DConvolution/conv5x5_3a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp2-->|n x 28 x 28 x 192|2DMaxPooling/maxp_3a;
    style 2DMaxPooling/maxp_3a fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_3a-->|n x 28 x 28 x 192|2DConvolution/pool_proj_3a;
    style 2DConvolution/pool_proj_3a fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_3a-->|n x 28 x 28 x 64|Concatenate/conc1_3a;
    2DConvolution/conv3x3_3a-->|n x 28 x 28 x 128|Concatenate/conc1_3a;
    2DConvolution/conv5x5_3a-->|n x 28 x 28 x 32|Concatenate/conc1_3a;
    2DConvolution/pool_proj_3a-->|n x 28 x 28 x 32|Concatenate/conc1_3a;
    style Concatenate/conc1_3a fill:#edbb99, stroke:#626262;
    Concatenate/conc1_3a-->|n x 28 x 28 x 256|2DConvolution/conv1x1_3b;
    style 2DConvolution/conv1x1_3b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_3a-->|n x 28 x 28 x 256|2DConvolution/reduce3x3_3b;
    style 2DConvolution/reduce3x3_3b fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_3b-->|n x 28 x 28 x 128|2DConvolution/conv3x3_3b;
    style 2DConvolution/conv3x3_3b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_3a-->|n x 28 x 28 x 256|2DConvolution/reduce5x5_3b;
    style 2DConvolution/reduce5x5_3b fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_3b-->|n x 28 x 28 x 128|2DConvolution/conv5x5_3b;
    style 2DConvolution/conv5x5_3b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_3a-->|n x 28 x 28 x 256|2DMaxPooling/maxp_3b;
    style 2DMaxPooling/maxp_3b fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_3b-->|n x 28 x 28 x 256|2DConvolution/pool_proj_3b;
    style 2DConvolution/pool_proj_3b fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_3b-->|n x 28 x 28 x 128|Concatenate/conc1_3b;
    2DConvolution/conv3x3_3b-->|n x 28 x 28 x 192|Concatenate/conc1_3b;
    2DConvolution/conv5x5_3b-->|n x 28 x 28 x 96|Concatenate/conc1_3b;
    2DConvolution/pool_proj_3b-->|n x 28 x 28 x 64|Concatenate/conc1_3b;
    style Concatenate/conc1_3b fill:#edbb99, stroke:#626262;
    Concatenate/conc1_3b-->|n x 28 x 28 x 480|2DMaxPooling/maxp3;
    style 2DMaxPooling/maxp3 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp3-->|n x 14 x 14 x 480|2DConvolution/conv1x1_4a;
    style 2DConvolution/conv1x1_4a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp3-->|n x 14 x 14 x 480|2DConvolution/reduce3x3_4a;
    style 2DConvolution/reduce3x3_4a fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_4a-->|n x 14 x 14 x 96|2DConvolution/conv3x3_4a;
    style 2DConvolution/conv3x3_4a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp3-->|n x 14 x 14 x 480|2DConvolution/reduce5x5_4a;
    style 2DConvolution/reduce5x5_4a fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_4a-->|n x 14 x 14 x 96|2DConvolution/conv5x5_4a;
    style 2DConvolution/conv5x5_4a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp3-->|n x 14 x 14 x 480|2DMaxPooling/maxp_4a;
    style 2DMaxPooling/maxp_4a fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_4a-->|n x 14 x 14 x 480|2DConvolution/pool_proj_4a;
    style 2DConvolution/pool_proj_4a fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_4a-->|n x 14 x 14 x 192|Concatenate/conc1_4a;
    2DConvolution/conv3x3_4a-->|n x 14 x 14 x 208|Concatenate/conc1_4a;
    2DConvolution/conv5x5_4a-->|n x 14 x 14 x 48|Concatenate/conc1_4a;
    2DConvolution/pool_proj_4a-->|n x 14 x 14 x 64|Concatenate/conc1_4a;
    style Concatenate/conc1_4a fill:#edbb99, stroke:#626262;
    Concatenate/conc1_4a-->|n x 14 x 14 x 512|2DAveragePooling/aux1_avg;
    style 2DAveragePooling/aux1_avg fill:#a9dfbf, stroke:#626262;
    2DAveragePooling/aux1_avg-->|n x 4 x 4 x 512|2DConvolution/aux1_conv1x1;
    style 2DConvolution/aux1_conv1x1 fill:#a9cce3, stroke:#626262;
    2DConvolution/aux1_conv1x1-->|n x 4 x 4 x 128|Flatten/aux1_flat;
    style Flatten/aux1_flat fill:#fad7a0, stroke:#626262;
    Flatten/aux1_flat-->|n x 2048|DenseLayer/aux1_dense1;
    style DenseLayer/aux1_dense1 fill:#a9cce3, stroke:#626262;
    DenseLayer/aux1_dense1-->|n x 1024|Dropout/aux1_do;
    style Dropout/aux1_do fill:#a3e4d7, stroke:#626262;
    Dropout/aux1_do-->|n x 1024|DenseLayer/aux1_dense2;
    style DenseLayer/aux1_dense2 fill:#a9cce3, stroke:#626262;
    DenseLayer/aux1_dense2-->|n x 1000|SoftmaxActivation/aux_head1;
    style SoftmaxActivation/aux_head1 fill:#d2b4de, stroke:#626262;
    Concatenate/conc1_4a-->|n x 14 x 14 x 512|2DConvolution/conv1x1_4b;
    style 2DConvolution/conv1x1_4b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4a-->|n x 14 x 14 x 512|2DConvolution/reduce3x3_4b;
    style 2DConvolution/reduce3x3_4b fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_4b-->|n x 14 x 14 x 112|2DConvolution/conv3x3_4b;
    style 2DConvolution/conv3x3_4b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4a-->|n x 14 x 14 x 512|2DConvolution/reduce5x5_4b;
    style 2DConvolution/reduce5x5_4b fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_4b-->|n x 14 x 14 x 112|2DConvolution/conv5x5_4b;
    style 2DConvolution/conv5x5_4b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4a-->|n x 14 x 14 x 512|2DMaxPooling/maxp_4b;
    style 2DMaxPooling/maxp_4b fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_4b-->|n x 14 x 14 x 512|2DConvolution/pool_proj_4b;
    style 2DConvolution/pool_proj_4b fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_4b-->|n x 14 x 14 x 160|Concatenate/conc1_4b;
    2DConvolution/conv3x3_4b-->|n x 14 x 14 x 224|Concatenate/conc1_4b;
    2DConvolution/conv5x5_4b-->|n x 14 x 14 x 64|Concatenate/conc1_4b;
    2DConvolution/pool_proj_4b-->|n x 14 x 14 x 64|Concatenate/conc1_4b;
    style Concatenate/conc1_4b fill:#edbb99, stroke:#626262;
    Concatenate/conc1_4b-->|n x 14 x 14 x 512|2DConvolution/conv1x1_4c;
    style 2DConvolution/conv1x1_4c fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4b-->|n x 14 x 14 x 512|2DConvolution/reduce3x3_4c;
    style 2DConvolution/reduce3x3_4c fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_4c-->|n x 14 x 14 x 128|2DConvolution/conv3x3_4c;
    style 2DConvolution/conv3x3_4c fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4b-->|n x 14 x 14 x 512|2DConvolution/reduce5x5_4c;
    style 2DConvolution/reduce5x5_4c fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_4c-->|n x 14 x 14 x 128|2DConvolution/conv5x5_4c;
    style 2DConvolution/conv5x5_4c fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4b-->|n x 14 x 14 x 512|2DMaxPooling/maxp_4c;
    style 2DMaxPooling/maxp_4c fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_4c-->|n x 14 x 14 x 512|2DConvolution/pool_proj_4c;
    style 2DConvolution/pool_proj_4c fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_4c-->|n x 14 x 14 x 128|Concatenate/conc1_4c;
    2DConvolution/conv3x3_4c-->|n x 14 x 14 x 256|Concatenate/conc1_4c;
    2DConvolution/conv5x5_4c-->|n x 14 x 14 x 64|Concatenate/conc1_4c;
    2DConvolution/pool_proj_4c-->|n x 14 x 14 x 64|Concatenate/conc1_4c;
    style Concatenate/conc1_4c fill:#edbb99, stroke:#626262;
    Concatenate/conc1_4c-->|n x 14 x 14 x 512|2DConvolution/conv1x1_4d;
    style 2DConvolution/conv1x1_4d fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4c-->|n x 14 x 14 x 512|2DConvolution/reduce3x3_4d;
    style 2DConvolution/reduce3x3_4d fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_4d-->|n x 14 x 14 x 144|2DConvolution/conv3x3_4d;
    style 2DConvolution/conv3x3_4d fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4c-->|n x 14 x 14 x 512|2DConvolution/reduce5x5_4d;
    style 2DConvolution/reduce5x5_4d fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_4d-->|n x 14 x 14 x 144|2DConvolution/conv5x5_4d;
    style 2DConvolution/conv5x5_4d fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4c-->|n x 14 x 14 x 512|2DMaxPooling/maxp_4d;
    style 2DMaxPooling/maxp_4d fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_4d-->|n x 14 x 14 x 512|2DConvolution/pool_proj_4d;
    style 2DConvolution/pool_proj_4d fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_4d-->|n x 14 x 14 x 112|Concatenate/conc1_4d;
    2DConvolution/conv3x3_4d-->|n x 14 x 14 x 288|Concatenate/conc1_4d;
    2DConvolution/conv5x5_4d-->|n x 14 x 14 x 64|Concatenate/conc1_4d;
    2DConvolution/pool_proj_4d-->|n x 14 x 14 x 64|Concatenate/conc1_4d;
    style Concatenate/conc1_4d fill:#edbb99, stroke:#626262;
    Concatenate/conc1_4d-->|n x 14 x 14 x 528|2DAveragePooling/aux2_avg;
    style 2DAveragePooling/aux2_avg fill:#a9dfbf, stroke:#626262;
    2DAveragePooling/aux2_avg-->|n x 4 x 4 x 528|2DConvolution/aux2_conv1x1;
    style 2DConvolution/aux2_conv1x1 fill:#a9cce3, stroke:#626262;
    2DConvolution/aux2_conv1x1-->|n x 4 x 4 x 128|Flatten/aux2_flat;
    style Flatten/aux2_flat fill:#fad7a0, stroke:#626262;
    Flatten/aux2_flat-->|n x 2048|DenseLayer/aux2_dense1;
    style DenseLayer/aux2_dense1 fill:#a9cce3, stroke:#626262;
    DenseLayer/aux2_dense1-->|n x 1024|Dropout/aux2_do;
    style Dropout/aux2_do fill:#a3e4d7, stroke:#626262;
    Dropout/aux2_do-->|n x 1024|DenseLayer/aux2_dense2;
    style DenseLayer/aux2_dense2 fill:#a9cce3, stroke:#626262;
    DenseLayer/aux2_dense2-->|n x 1000|SoftmaxActivation/aux_head2;
    style SoftmaxActivation/aux_head2 fill:#d2b4de, stroke:#626262;
    Concatenate/conc1_4d-->|n x 14 x 14 x 528|2DConvolution/conv1x1_4e;
    style 2DConvolution/conv1x1_4e fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4d-->|n x 14 x 14 x 528|2DConvolution/reduce3x3_4e;
    style 2DConvolution/reduce3x3_4e fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_4e-->|n x 14 x 14 x 160|2DConvolution/conv3x3_4e;
    style 2DConvolution/conv3x3_4e fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4d-->|n x 14 x 14 x 528|2DConvolution/reduce5x5_4e;
    style 2DConvolution/reduce5x5_4e fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_4e-->|n x 14 x 14 x 160|2DConvolution/conv5x5_4e;
    style 2DConvolution/conv5x5_4e fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_4d-->|n x 14 x 14 x 528|2DMaxPooling/maxp_4e;
    style 2DMaxPooling/maxp_4e fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_4e-->|n x 14 x 14 x 528|2DConvolution/pool_proj_4e;
    style 2DConvolution/pool_proj_4e fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_4e-->|n x 14 x 14 x 256|Concatenate/conc1_4e;
    2DConvolution/conv3x3_4e-->|n x 14 x 14 x 320|Concatenate/conc1_4e;
    2DConvolution/conv5x5_4e-->|n x 14 x 14 x 128|Concatenate/conc1_4e;
    2DConvolution/pool_proj_4e-->|n x 14 x 14 x 128|Concatenate/conc1_4e;
    style Concatenate/conc1_4e fill:#edbb99, stroke:#626262;
    Concatenate/conc1_4e-->|n x 14 x 14 x 832|2DMaxPooling/maxp4;
    style 2DMaxPooling/maxp4 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp4-->|n x 7 x 7 x 832|2DConvolution/conv1x1_5a;
    style 2DConvolution/conv1x1_5a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp4-->|n x 7 x 7 x 832|2DConvolution/reduce3x3_5a;
    style 2DConvolution/reduce3x3_5a fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_5a-->|n x 7 x 7 x 160|2DConvolution/conv3x3_5a;
    style 2DConvolution/conv3x3_5a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp4-->|n x 7 x 7 x 832|2DConvolution/reduce5x5_5a;
    style 2DConvolution/reduce5x5_5a fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_5a-->|n x 7 x 7 x 160|2DConvolution/conv5x5_5a;
    style 2DConvolution/conv5x5_5a fill:#a9cce3, stroke:#626262;
    2DMaxPooling/maxp4-->|n x 7 x 7 x 832|2DMaxPooling/maxp_5a;
    style 2DMaxPooling/maxp_5a fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_5a-->|n x 7 x 7 x 832|2DConvolution/pool_proj_5a;
    style 2DConvolution/pool_proj_5a fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_5a-->|n x 7 x 7 x 256|Concatenate/conc1_5a;
    2DConvolution/conv3x3_5a-->|n x 7 x 7 x 320|Concatenate/conc1_5a;
    2DConvolution/conv5x5_5a-->|n x 7 x 7 x 128|Concatenate/conc1_5a;
    2DConvolution/pool_proj_5a-->|n x 7 x 7 x 128|Concatenate/conc1_5a;
    style Concatenate/conc1_5a fill:#edbb99, stroke:#626262;
    Concatenate/conc1_5a-->|n x 7 x 7 x 832|2DConvolution/conv1x1_5b;
    style 2DConvolution/conv1x1_5b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_5a-->|n x 7 x 7 x 832|2DConvolution/reduce3x3_5b;
    style 2DConvolution/reduce3x3_5b fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce3x3_5b-->|n x 7 x 7 x 192|2DConvolution/conv3x3_5b;
    style 2DConvolution/conv3x3_5b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_5a-->|n x 7 x 7 x 832|2DConvolution/reduce5x5_5b;
    style 2DConvolution/reduce5x5_5b fill:#a9cce3, stroke:#626262;
    2DConvolution/reduce5x5_5b-->|n x 7 x 7 x 192|2DConvolution/conv5x5_5b;
    style 2DConvolution/conv5x5_5b fill:#a9cce3, stroke:#626262;
    Concatenate/conc1_5a-->|n x 7 x 7 x 832|2DMaxPooling/maxp_5b;
    style 2DMaxPooling/maxp_5b fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/maxp_5b-->|n x 7 x 7 x 832|2DConvolution/pool_proj_5b;
    style 2DConvolution/pool_proj_5b fill:#a9cce3, stroke:#626262;
    2DConvolution/conv1x1_5b-->|n x 7 x 7 x 384|Concatenate/conc1_5b;
    2DConvolution/conv3x3_5b-->|n x 7 x 7 x 384|Concatenate/conc1_5b;
    2DConvolution/conv5x5_5b-->|n x 7 x 7 x 128|Concatenate/conc1_5b;
    2DConvolution/pool_proj_5b-->|n x 7 x 7 x 128|Concatenate/conc1_5b;
    style Concatenate/conc1_5b fill:#edbb99, stroke:#626262;
    Concatenate/conc1_5b-->|n x 7 x 7 x 1024|2DAveragePooling/avg;
    style 2DAveragePooling/avg fill:#a9dfbf, stroke:#626262;
    2DAveragePooling/avg-->|n x 1 x 1 x 1024|Flatten/flat;
    style Flatten/flat fill:#fad7a0, stroke:#626262;
    Flatten/flat-->|n x 1024|Dropout/do;
    style Dropout/do fill:#a3e4d7, stroke:#626262;
    Dropout/do-->|n x 1024|DenseLayer/linear;
    style DenseLayer/linear fill:#a9cce3, stroke:#626262;
    DenseLayer/linear-->|n x 1000|SoftmaxActivation/p_classes;
    style SoftmaxActivation/p_classes fill:#d2b4de, stroke:#626262;
```


### Model Training

``test_project/training.py`` contains the code for training the model. The training script was generated for input and target data that have the following specifications:

#### Input data
```mermaid
graph BT;
    HDF5File/source_file_images-->|"['images'][:]"|InputData/images;
```

#### Target data
```mermaid
graph BT;
    CSVFile/source_file_aux_target1-->|"[:, -1]"|TargetData/aux_target1;
    CSVFile/source_file_aux_target2-->|"[:, -1]"|TargetData/aux_target2;
    HDF5File/source_file_classes-->|"['labels'][:]"|TargetData/classes;
```


Additional training options are exposed via a commandline interface. You can get an overview of these options by calling the script with the flag ``-h`` or ``--help``:

```bash
python training.py -h
usage: training.py [-h] [--source_file_1 SOURCE_FILE_1]
                   [--source_file_2 SOURCE_FILE_2]
                   [--w_loss_classes W_LOSS_CLASSES]
                   [--w_loss_aux_target1 W_LOSS_AUX_TARGET1]
                   [--w_loss_aux_target2 W_LOSS_AUX_TARGET2]
                   [--learning_rate LEARNING_RATE] [--beta1 BETA1]
                   [--beta2 BETA2] [--epsilon EPSILON]
                   [--checkpoint_file CHECKPOINT_FILE] [--mbsize MBSIZE]
                   [--num_epochs NUM_EPOCHS]

Command-line interface for configuring and training the generated model

optional arguments:
  -h, --help            show this help message and exit
  --source_file_1 SOURCE_FILE_1
                        The source file which contains the data for the
                        sources: `images`, `classes`. (default:
                        './yourpath.hdf5')
  --source_file_2 SOURCE_FILE_2
                        The source file which contains the data for the
                        sources: `aux_target1`, `aux_target2`. (default:
                        './yourpath.npy')
  --w_loss_classes W_LOSS_CLASSES
                        Weight of loss on target `classes` in overall cost
                        function. (default: 1.0)
  --w_loss_aux_target1 W_LOSS_AUX_TARGET1
                        Weight of loss on target `aux_target1` in overall cost
                        function. (default: 0.3)
  --w_loss_aux_target2 W_LOSS_AUX_TARGET2
                        Weight of loss on target `aux_target2` in overall cost
                        function. (default: 0.3)
  --learning_rate LEARNING_RATE
                        ADAM optimizer: Learning rate. (default: 0.001)
  --beta1 BETA1         ADAM optimizer: Exponential decay rate for 1st moment
                        estimates. (default: 0.99)
  --beta2 BETA2         ADAM optimizer: Exponential decay rate for 2nd moment
                        estimates. (default: 0.95)
  --epsilon EPSILON     ADAM optimizer: Small constant for numerical
                        stability. (default: 1e-08)
  --checkpoint_file CHECKPOINT_FILE
                        Set the checkpoint file path and name used for
                        checkpointing the model on disk. This path will only
                        be used to paste the string into the generated code.
                        (default: './')
  --mbsize MBSIZE       The mini-batch size determines how many examples are
                        sampled from the dataset to compute the model error
                        and the updates applied to the model parameters in
                        every iteration of the training.It is common practice
                        to choose "powers of two", e.g. 32, 64, 128, 256.
                        (default: 128)
  --num_epochs NUM_EPOCHS
                        The maximum number of epochs (one epoch = one
                        iteration through the whole data set) for model
                        training. (default: 100)

```

### Model Inference
``test_project/inference.py`` contains the code for applying the trained model to input data for prediction. The inference script was generated for input data that has the following specifications:

#### Input data

```mermaid
graph BT;
    HDF5File/source_file_images-->|"['images'][:]"|InputData/images;
```


Additional inference options are exposed by a commandline interface. You can get an overview of these options by calling the script with the flag ``-h`` or ``--help``:

```bash
python inference.py -h
usage: inference.py [-h] [--source_file_1 SOURCE_FILE_1]
                    [--checkpoint_file CHECKPOINT_FILE] [--clear_devices]
                    [--savepath SAVEPATH]

Command-line interface for applying the trained model on new data.

optional arguments:
  -h, --help            show this help message and exit
  --source_file_1 SOURCE_FILE_1
                        The source file which contains the data for the
                        sources: `images`. (default: './yourpath.hdf5')
  --checkpoint_file CHECKPOINT_FILE
                        The path to the checkpoint file the model is restored
                        from. (default: './')
  --clear_devices       Set to True if the graph device placements should be
                        reseted. (default: False)
  --savepath SAVEPATH   Destination where the predictions are stored. By
                        default, the predictions are simply printed to STDOUT.
                        (default: 'None')

```
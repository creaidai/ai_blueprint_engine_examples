"""
Preprocess the data from the Titanic: Machine Learning from Disaster Kaggle
Competition:

https://www.kaggle.com/c/titanic/

Preprocessing steps:
* removed columns that are of low quality or do not seem valuable.
* filled missing values.
* encoded categories in text form to categorical indices.

"""


from __future__ import print_function
from builtins import range

import numpy as np
import pandas as pd


# Fix the random number generator.
rng = np.random.RandomState(123213)


def main():
    train_data = pd.read_csv('./data/train.csv', index_col=0)
    test_data = pd.read_csv('./data/test.csv', index_col=0)

    # Show an overview of the data.
    print('Before preprocessing:')
    print('---------------------')
    inspect_data(train_data)
    inspect_data(test_data)

    # Manually selection features that have a reasonable content and quality.
    keep_features = [
        'Survived', 'Pclass', 'Sex', 'Age', 'SibSp', 'Parch', 'Fare',
        'Embarked']

    # Select the columns from the data sets.
    train_data = train_data[keep_features]
    test_data = test_data[keep_features[1:]]  # Test set has no 'Survived' col.

    # Fill the nan values for 'Age' and 'Fare'.
    fill_numerical_nan(train_data, test_data, 'Age')
    fill_numerical_nan(train_data, test_data, 'Fare')

    # Fill categorical nans for 'Embarked'.
    fill_categorical_nan(train_data, test_data, 'Embarked')

    # Encode categorical columns as categorical indices.
    encode_categorical(train_data, test_data, 'Pclass')
    encode_categorical(train_data, test_data, 'Embarked')
    encode_categorical(train_data, test_data, 'Sex')

    # Show an overview of the data again.
    print()
    print('After preprocessing:')
    print('--------------------')
    inspect_data(train_data)
    inspect_data(test_data)

    # Store the preprocessed data on disk.
    train_data.to_csv('./data/train_preprocessed.csv', index=False)
    test_data.to_csv('./data/test_preprocessed.csv', index=False)

    # Make an extra model selection dataset.
    num_examples = train_data.shape[0]
    sort_idcs = np.random.permutation(np.arange(num_examples))
    train_data = train_data.iloc[sort_idcs]
    train_data.iloc[int(num_examples * 0.1):].to_csv(
        './data/train_train_preprocessed.csv', index=False)
    train_data.iloc[:int(num_examples * 0.1)].to_csv(
        './data/train_test_preprocessed.csv', index=False)


def inspect_data(data):
    """Shows an overview of the data set."""
    # Check how many examples the data has.
    num_examples = data.shape[0]
    print('Number of examples: {:3}'.format(num_examples))

    categorical_features = ['Survived', 'Pclass', 'Sex', 'Embarked']
    for i in range(data.shape[1]):
        # Get the name of the column.
        feature_name = data.columns[i]
        # Count the number of entries that are not NaN.
        num_valid = data.iloc[:, i].count()
        # Compute the number of unique elements ignoring NaN.
        num_unique = data.iloc[:, i].nunique()
        # Get the data type of the feature.
        feature_dtype = data.iloc[:, i].dtype

        # Get an overview on the content of the data.
        if feature_name in categorical_features:
            unique_elements = np.sort(data.iloc[:, i].dropna().unique())
            print('Col idx: {}, Feature name: {:8}, Valid entries: {:4d}/{:3d}'
                  ', Unique elements: {:3d}, Data type: {},'
                  ' Elements: {}'.format(
                      i, feature_name, num_valid, num_examples, num_unique,
                      feature_dtype, unique_elements))
        else:
            print('Col idx: {}, Feature name: {:8}, Valid entries: {:4d}/{:3d}'
                  ', Unique elements: {:3d}, Data type: {}'.format(
                      i, feature_name, num_valid, num_examples, num_unique,
                      feature_dtype))


# Next, we fill the missing values and define some functions for that.
def fill_categorical_nan(train_data, test_data, feature_name):
    """Fills the NaN values by randomly choosing a known category."""
    # Get the unique elements from the training set.
    unique_elements = train_data[feature_name].dropna().unique()

    # Fill the nan values.
    for data in [train_data, test_data]:
        nan_indices = data[feature_name].index[
            data[feature_name].apply(pd.isnull)]
        fill_values = rng.choice(unique_elements, size=len(nan_indices))
        data[feature_name].loc[nan_indices] = fill_values


def fill_numerical_nan(train_data, test_data, feature_name):
    """Fills the NaN values by the mean."""
    # Compute the mean from the training set.
    fill_value = train_data[feature_name].mean()

    # Fill the nan values.
    for data in [train_data, test_data]:
        data[feature_name].fillna(fill_value, inplace=True)


def encode_categorical(train_data, test_data, feature_name):
    # Get the unique elements from the training set.
    unique_elements = train_data[feature_name].dropna().unique()

    for data in [train_data, test_data]:
        element_indices = []
        for unique_element in unique_elements:
            # Collect all row indices the element occurs in the data.
            element_indices += [data[feature_name].index[
                data[feature_name].apply(lambda x: x == unique_element)]]

        # Encode the elements with a category index.
        for element_encoding, element_indices_i in enumerate(element_indices):
            data.loc[element_indices_i, feature_name] = element_encoding

        data[feature_name] = data[feature_name].astype(np.int)


if __name__ == '__main__':
    main()

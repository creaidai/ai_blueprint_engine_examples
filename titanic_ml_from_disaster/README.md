# titanic_ml_from_disaster

## Project overview

Your project description.

## Setup

### Dependencies

* h5py
* numpy
* pandas
* tensorflow==1.5

### Project Setup

For running the code and/or continuing development, you need to add the project to your ``PYTHONPATH`` in order for Python to find module imports inside the project directory.

#### Linux / MacOS
We assume in the following that the project is located at ``/home/user/projects``. Please substitute this path by the actual path of your project directory.
The project path can be assigned to ``PYTHONPATH`` for the duration of executing the subsequent command.

```bash
$ PYTHONPATH=/home/user/projects/titanic_ml_from_disaster python training.py
```

We recommend to set the ``PYTHONPATH`` permanently. In case you run Python from within an editor, you can typically set the ``PYTHONPATH`` in the editor configuration. Please consult the documentation of your editor. If you are executing Python from the shell, open the ``$HOME/.bashrc`` file with your favorite editor and add the following line at the bottom of the file:

```bash
$ export PYTHONPATH=$PYTHONPATH:/home/user/projects/titanic_ml_from_disaster
```
To make the changes effective, open a new shell or run the following command in the current shell:

```bash
$ source $HOME/.bashrc
```

#### Windows
On Windows 7, environment variables can be set at ``My Computer > Properties > Advanced System Settings > Environment Variables``. Search under ``System variables`` for ``PYTHONPATH``. If this entry exists, click ``Edit...`` and append the path of the project. If this entry does not exist, click ``New...``, set ``Variable name`` to ``PYTHONPATH`` and ``Variable value`` to the path of the project.

On Windows 10, the environment variables can be se at ``Start > Settings > System > About > System info > Advanced system settings > Environment variables``. Search under ``System variables`` for ``PYTHONPATH``. If this entry exists, click ``Edit...`` and append the path of the project. If this entry does not exist, click ``New...``, set ``Variable name`` to ``PYTHONPATH`` and ``Variable value`` to the path of the project.

If you run Python from within an editor, please consult its documentation to find out how to set the ``PYTHONPATH`` environment variable.

#### Docker

Docker images to run the generated code on both CPU and GPU can be built using the provided Dockerfiles. In order to build a Docker image with CPU-only support, execute the following command:

```bash
$ docker build -t titanic_ml_from_disaster:latest .
```

Similarly, a Docker image with GPU support can be built by executing the following command:

```bash
$ docker build -t titanic_ml_from_disaster:latest -f Dockerfile.gpu .
```

In order to run, e.g., the training script inside a Docker container, data files need to be made available inside the container using [Docker volumes](https://docs.docker.com/storage/volumes/#choose-the--v-or---mount-flag). Assuming `/path/to/your/data` is the directory where training data is stored, `/path/inside/container` is the path where this directory is mounted inside the container, and there is the name of the input source variable is `source_file_1`, then the training script can be executed inside a Docker container as follows:

```bash
$ docker run --rm -v /path/to/data:/path/in/container \
        titanic_ml_from_disaster:latest \
            python titanic_ml_from_disaster/training.py \
                --source_file_1 /path/inside/container/data.csv
```

## Generated Code Overview
In the following a description of the generated code and its basic content is provided. For some of the documentation the Markdown [mermaid](https://mermaidjs.github.io) extension is required to visualize graphs. Alternatively, you can use the [mermaid live editor](https://mermaidjs.github.io/mermaid-live-editor).

### Blueprint

#### Model summary
* A 3 layer neural network that predicts 1 target source from 5 input sources.

#### Model inputs
* `age_and_fare` : Float Vector (n x 2)
* `sibsp_and_parch` : Count Vector (n x 2)
* `gender` : Single Binary Value (n)
* `pclass` : Single Categorical Value (n)
* `embarked` : Single Categorical Value (n)

#### Model targets
* `survived` : Binary Class (n)

#### Data Preprocessing
```mermaid
graph BT;
    InputData/age_and_fare-->|n x 2|Standardization/normed_af;
    style Standardization/normed_af fill:#a2d9ce, stroke:#626262;
    Standardization/normed_af-->|n x 2|NNInput/input_af;
    style NNInput/input_af fill:#d5dbdb, stroke:#626262;
    InputData/sibsp_and_parch-->|n x 2|LogTransform/log_sp;
    style LogTransform/log_sp fill:#a2d9ce, stroke:#626262;
    LogTransform/log_sp-->|n x 2|NNInput/input_sp;
    style NNInput/input_sp fill:#d5dbdb, stroke:#626262;
    InputData/gender-->|n|NNInput/input_gender_;
    style NNInput/input_gender_ fill:#d5dbdb, stroke:#626262;
    InputData/pclass-->|n|NNInput/input_pclass;
    style NNInput/input_pclass fill:#d5dbdb, stroke:#626262;
    InputData/embarked-->|n|NNInput/input_embarked;
    style NNInput/input_embarked fill:#d5dbdb, stroke:#626262;
    TargetData/survived-->|n|NNTarget/loss_survived;
    style NNTarget/loss_survived fill:#abb2b9, stroke:#626262;
```

#### Model Architecture
```mermaid
graph BT;
    NNInput/input_pclass-->|n|EmbeddingLayer/emb_pclass;
    style EmbeddingLayer/emb_pclass fill:#a9dfbf, stroke:#626262;
    NNInput/input_embarked-->|n|EmbeddingLayer/emb_embarked;
    style EmbeddingLayer/emb_embarked fill:#a9dfbf, stroke:#626262;
    NNInput/input_af-->|n x 2|Concatenate/conc_0;
    NNInput/input_sp-->|n x 2|Concatenate/conc_0;
    NNInput/input_gender_-->|n x 1|Concatenate/conc_0;
    EmbeddingLayer/emb_pclass-->|n x 10|Concatenate/conc_0;
    EmbeddingLayer/emb_embarked-->|n x 10|Concatenate/conc_0;
    style Concatenate/conc_0 fill:#edbb99, stroke:#626262;
    Concatenate/conc_0-->|n x 25|BatchNormalization/h_bn_0;
    style BatchNormalization/h_bn_0 fill:#a3e4d7, stroke:#626262;
    BatchNormalization/h_bn_0-->|n x 25|DenseLayer/h_dense_0;
    style DenseLayer/h_dense_0 fill:#a9cce3, stroke:#626262;
    DenseLayer/h_dense_0-->|n x 20|BatchNormalization/h_bn_1;
    style BatchNormalization/h_bn_1 fill:#a3e4d7, stroke:#626262;
    BatchNormalization/h_bn_1-->|n x 20|DenseLayer/h_dense_1;
    style DenseLayer/h_dense_1 fill:#a9cce3, stroke:#626262;
    DenseLayer/h_dense_1-->|n x 20|BatchNormalization/h_bn_2;
    style BatchNormalization/h_bn_2 fill:#a3e4d7, stroke:#626262;
    BatchNormalization/h_bn_2-->|n x 20|DenseLayer/logits;
    style DenseLayer/logits fill:#a9cce3, stroke:#626262;
    DenseLayer/logits-->|n x 1|SigmoidActivation/model_output;
    style SigmoidActivation/model_output fill:#d2b4de, stroke:#626262;
```


### Model Training

``test_project/training.py`` contains the code for training the model. The training script was generated for input and target data that have the following specifications:

#### Input data
```mermaid
graph BT;
    CSVFile/source_file_age_and_fare-->|"[:, [3, 6]]"|InputData/age_and_fare;
    CSVFile/source_file_sibsp_and_parch-->|"[:, [4, 5]]"|InputData/sibsp_and_parch;
    CSVFile/source_file_gender-->|"[:, 2]"|InputData/gender;
    CSVFile/source_file_pclass-->|"[:, 1]"|InputData/pclass;
    CSVFile/source_file_embarked-->|"[:, 7]"|InputData/embarked;
```

#### Target data
```mermaid
graph BT;
    CSVFile/source_file_survived-->|"[:, 0]"|TargetData/survived;
```


Additional training options are exposed via a commandline interface. You can get an overview of these options by calling the script with the flag ``-h`` or ``--help``:

```bash
python training.py -h
usage: training.py [-h] [--source_file_1 SOURCE_FILE_1] [--l2_reg L2_REG]
                   [--learning_rate LEARNING_RATE] [--beta1 BETA1]
                   [--beta2 BETA2] [--epsilon EPSILON]
                   [--checkpoint_file CHECKPOINT_FILE] [--mbsize MBSIZE]
                   [--num_epochs NUM_EPOCHS] [--max_patience MAX_PATIENCE]

Command-line interface for configuring and training the generated model

optional arguments:
  -h, --help            show this help message and exit
  --source_file_1 SOURCE_FILE_1
                        The source file which contains the data for the
                        sources: `age_and_fare`, `sibsp_and_parch`, `gender`,
                        `pclass`, `embarked`, `survived`. (default:
                        './data/train_preprocessed.csv')
  --l2_reg L2_REG       Weight Decay: L2 Penalty Factor. (default: 0.0001)
  --learning_rate LEARNING_RATE
                        ADAM optimizer: Learning rate. (default: 0.001)
  --beta1 BETA1         ADAM optimizer: Exponential decay rate for 1st moment
                        estimates. (default: 0.99)
  --beta2 BETA2         ADAM optimizer: Exponential decay rate for 2nd moment
                        estimates. (default: 0.95)
  --epsilon EPSILON     ADAM optimizer: Small constant for numerical
                        stability. (default: 1e-08)
  --checkpoint_file CHECKPOINT_FILE
                        Set the checkpoint file path and name used for
                        checkpointing the model on disk. This path will only
                        be used to paste the string into the generated code.
                        (default: './checkpoints/')
  --mbsize MBSIZE       The mini-batch size determines how many examples are
                        sampled from the dataset to compute the model error
                        and the updates applied to the model parameters in
                        every iteration of the training.It is common practice
                        to choose "powers of two", e.g. 32, 64, 128, 256.
                        (default: 32)
  --num_epochs NUM_EPOCHS
                        The maximum number of epochs (one epoch = one
                        iteration through the whole data set) for model
                        training. (default: 100)
  --max_patience MAX_PATIENCE
                        The number of consecutive times the computed estimate
                        of the validation error has not improved after which
                        training is stopped. (default: 10)

```

### Model Inference
``test_project/inference.py`` contains the code for applying the trained model to input data for prediction. The inference script was generated for input data that has the following specifications:

#### Input data

```mermaid
graph BT;
    CSVFile/source_file_age_and_fare-->|"[:, [3, 6]]"|InputData/age_and_fare;
    CSVFile/source_file_sibsp_and_parch-->|"[:, [4, 5]]"|InputData/sibsp_and_parch;
    CSVFile/source_file_gender-->|"[:, 2]"|InputData/gender;
    CSVFile/source_file_pclass-->|"[:, 1]"|InputData/pclass;
    CSVFile/source_file_embarked-->|"[:, 7]"|InputData/embarked;
```


Additional inference options are exposed by a commandline interface. You can get an overview of these options by calling the script with the flag ``-h`` or ``--help``:

```bash
python inference.py -h
usage: inference.py [-h] [--source_file_1 SOURCE_FILE_1]
                    [--checkpoint_file CHECKPOINT_FILE] [--clear_devices]
                    [--savepath SAVEPATH]

Command-line interface for applying the trained model on new data.

optional arguments:
  -h, --help            show this help message and exit
  --source_file_1 SOURCE_FILE_1
                        The source file which contains the data for the
                        sources: `age_and_fare`, `sibsp_and_parch`, `gender`,
                        `pclass`, `embarked`. (default:
                        './data/train_preprocessed.csv')
  --checkpoint_file CHECKPOINT_FILE
                        The path to the checkpoint file the model is restored
                        from. (default: './checkpoints/')
  --clear_devices       Set to True if the graph device placements should be
                        reseted. (default: False)
  --savepath SAVEPATH   Destination where the predictions are stored. By
                        default, the predictions are simply printed to STDOUT.
                        (default: 'None')

```
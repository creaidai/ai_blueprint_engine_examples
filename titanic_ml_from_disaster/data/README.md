# Data

You can download the data (train.csv and test.csv) from here:

[Titanic: Machine Learning form Disaster](https://www.kaggle.com/c/titanic)

**Note:** You have to join the competition to get access.

# vgg19

## Project overview

Your project description.

## Setup

### Dependencies

* h5py
* numpy
* tensorflow==1.5

### Project Setup

For running the code and/or continuing development, you need to add the project to your ``PYTHONPATH`` in order for Python to find module imports inside the project directory.

#### Linux / MacOS
We assume in the following that the project is located at ``/home/user/projects``. Please substitute this path by the actual path of your project directory.
The project path can be assigned to ``PYTHONPATH`` for the duration of executing the subsequent command.

```bash
$ PYTHONPATH=/home/user/projects/vgg19 python training.py
```

We recommend to set the ``PYTHONPATH`` permanently. In case you run Python from within an editor, you can typically set the ``PYTHONPATH`` in the editor configuration. Please consult the documentation of your editor. If you are executing Python from the shell, open the ``$HOME/.bashrc`` file with your favorite editor and add the following line at the bottom of the file:

```bash
$ export PYTHONPATH=$PYTHONPATH:/home/user/projects/vgg19
```
To make the changes effective, open a new shell or run the following command in the current shell:

```bash
$ source $HOME/.bashrc
```

#### Windows
On Windows 7, environment variables can be set at ``My Computer > Properties > Advanced System Settings > Environment Variables``. Search under ``System variables`` for ``PYTHONPATH``. If this entry exists, click ``Edit...`` and append the path of the project. If this entry does not exist, click ``New...``, set ``Variable name`` to ``PYTHONPATH`` and ``Variable value`` to the path of the project.

On Windows 10, the environment variables can be se at ``Start > Settings > System > About > System info > Advanced system settings > Environment variables``. Search under ``System variables`` for ``PYTHONPATH``. If this entry exists, click ``Edit...`` and append the path of the project. If this entry does not exist, click ``New...``, set ``Variable name`` to ``PYTHONPATH`` and ``Variable value`` to the path of the project.

If you run Python from within an editor, please consult its documentation to find out how to set the ``PYTHONPATH`` environment variable.

#### Docker

Docker images to run the generated code on both CPU and GPU can be built using the provided Dockerfiles. In order to build a Docker image with CPU-only support, execute the following command:

```bash
$ docker build -t vgg19:latest .
```

Similarly, a Docker image with GPU support can be built by executing the following command:

```bash
$ docker build -t vgg19:latest -f Dockerfile.gpu .
```

In order to run, e.g., the training script inside a Docker container, data files need to be made available inside the container using [Docker volumes](https://docs.docker.com/storage/volumes/#choose-the--v-or---mount-flag). Assuming `/path/to/your/data` is the directory where training data is stored, `/path/inside/container` is the path where this directory is mounted inside the container, and there is the name of the input source variable is `source_file_1`, then the training script can be executed inside a Docker container as follows:

```bash
$ docker run --rm -v /path/to/data:/path/in/container \
        vgg19:latest \
            python vgg19/training.py \
                --source_file_1 /path/inside/container/data.csv
```

## Generated Code Overview
In the following a description of the generated code and its basic content is provided. For some of the documentation the Markdown [mermaid](https://mermaidjs.github.io) extension is required to visualize graphs. Alternatively, you can use the [mermaid live editor](https://mermaidjs.github.io/mermaid-live-editor).

### Blueprint

#### Model summary
* A 19 layer neural network that predicts 1 target source from 1 input source.

#### Model inputs
* `images` : RGB Image (n x 224 x 224 x 3)

#### Model targets
* `classes` : Sparse Multi Class (n)

#### Data Preprocessing
```mermaid
graph BT;
    InputData/images-->|n x 224 x 224 x 3|NormalizeImages/normed_images;
    style NormalizeImages/normed_images fill:#a2d9ce, stroke:#626262;
    NormalizeImages/normed_images-->|n x 224 x 224 x 3|NNInput/input_;
    style NNInput/input_ fill:#d5dbdb, stroke:#626262;
    TargetData/classes-->|n|NNTarget/class_loss;
    style NNTarget/class_loss fill:#abb2b9, stroke:#626262;
```

#### Model Architecture
```mermaid
graph BT;
    NNInput/input_-->|n x 224 x 224 x 3|2DConvolution/output_conv_11;
    style 2DConvolution/output_conv_11 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_11-->|n x 224 x 224 x 64|2DConvolution/output_conv_12;
    style 2DConvolution/output_conv_12 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_12-->|n x 224 x 224 x 64|2DMaxPooling/output_pool_1;
    style 2DMaxPooling/output_pool_1 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/output_pool_1-->|n x 112 x 112 x 64|2DConvolution/output_conv_21;
    style 2DConvolution/output_conv_21 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_21-->|n x 112 x 112 x 128|2DConvolution/output_conv_22;
    style 2DConvolution/output_conv_22 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_22-->|n x 112 x 112 x 128|2DMaxPooling/output_pool_2;
    style 2DMaxPooling/output_pool_2 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/output_pool_2-->|n x 56 x 56 x 128|2DConvolution/output_conv_31;
    style 2DConvolution/output_conv_31 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_31-->|n x 56 x 56 x 256|2DConvolution/output_conv_32;
    style 2DConvolution/output_conv_32 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_32-->|n x 56 x 56 x 256|2DConvolution/output_conv_33;
    style 2DConvolution/output_conv_33 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_33-->|n x 56 x 56 x 256|2DConvolution/output_conv_34;
    style 2DConvolution/output_conv_34 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_34-->|n x 56 x 56 x 256|2DMaxPooling/output_pool_3;
    style 2DMaxPooling/output_pool_3 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/output_pool_3-->|n x 28 x 28 x 256|2DConvolution/output_conv_41;
    style 2DConvolution/output_conv_41 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_41-->|n x 28 x 28 x 512|2DConvolution/output_conv_42;
    style 2DConvolution/output_conv_42 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_42-->|n x 28 x 28 x 512|2DConvolution/output_conv_43;
    style 2DConvolution/output_conv_43 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_43-->|n x 28 x 28 x 512|2DConvolution/output_conv_44;
    style 2DConvolution/output_conv_44 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_44-->|n x 28 x 28 x 512|2DMaxPooling/output_pool_4;
    style 2DMaxPooling/output_pool_4 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/output_pool_4-->|n x 14 x 14 x 512|2DConvolution/output_conv_51;
    style 2DConvolution/output_conv_51 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_51-->|n x 14 x 14 x 512|2DConvolution/output_conv_52;
    style 2DConvolution/output_conv_52 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_52-->|n x 14 x 14 x 512|2DConvolution/output_conv_53;
    style 2DConvolution/output_conv_53 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_53-->|n x 14 x 14 x 512|2DConvolution/output_conv_54;
    style 2DConvolution/output_conv_54 fill:#a9cce3, stroke:#626262;
    2DConvolution/output_conv_54-->|n x 14 x 14 x 512|2DMaxPooling/output_pool_5;
    style 2DMaxPooling/output_pool_5 fill:#a9dfbf, stroke:#626262;
    2DMaxPooling/output_pool_5-->|n x 7 x 7 x 512|Flatten/flat;
    style Flatten/flat fill:#fad7a0, stroke:#626262;
    Flatten/flat-->|n x 25088|Dropout/output_dropout_1;
    style Dropout/output_dropout_1 fill:#a3e4d7, stroke:#626262;
    Dropout/output_dropout_1-->|n x 25088|DenseLayer/output_dense_1;
    style DenseLayer/output_dense_1 fill:#a9cce3, stroke:#626262;
    DenseLayer/output_dense_1-->|n x 4096|Dropout/output_dropout_2;
    style Dropout/output_dropout_2 fill:#a3e4d7, stroke:#626262;
    Dropout/output_dropout_2-->|n x 4096|DenseLayer/output_dense_2;
    style DenseLayer/output_dense_2 fill:#a9cce3, stroke:#626262;
    DenseLayer/output_dense_2-->|n x 4096|DenseLayer/logits;
    style DenseLayer/logits fill:#a9cce3, stroke:#626262;
    DenseLayer/logits-->|n x 1000|SoftmaxActivation/class_probs;
    style SoftmaxActivation/class_probs fill:#d2b4de, stroke:#626262;
```


### Model Training

``test_project/training.py`` contains the code for training the model. The training script was generated for input and target data that have the following specifications:

#### Input data
```mermaid
graph BT;
    HDF5File/source_file_images-->|"['images'][:]"|InputData/images;
```

#### Target data
```mermaid
graph BT;
    HDF5File/source_file_classes-->|"['labels'][:]"|TargetData/classes;
```


Additional training options are exposed via a commandline interface. You can get an overview of these options by calling the script with the flag ``-h`` or ``--help``:

```bash
python training.py -h
usage: training.py [-h] [--source_file_1 SOURCE_FILE_1]
                   [--learning_rate LEARNING_RATE] [--beta1 BETA1]
                   [--beta2 BETA2] [--epsilon EPSILON]
                   [--checkpoint_file CHECKPOINT_FILE] [--mbsize MBSIZE]
                   [--num_epochs NUM_EPOCHS]

Command-line interface for configuring and training the generated model

optional arguments:
  -h, --help            show this help message and exit
  --source_file_1 SOURCE_FILE_1
                        The source file which contains the data for the
                        sources: `images`, `classes`. (default:
                        './yourpath.hdf5')
  --learning_rate LEARNING_RATE
                        ADAM optimizer: Learning rate. (default: 0.001)
  --beta1 BETA1         ADAM optimizer: Exponential decay rate for 1st moment
                        estimates. (default: 0.99)
  --beta2 BETA2         ADAM optimizer: Exponential decay rate for 2nd moment
                        estimates. (default: 0.95)
  --epsilon EPSILON     ADAM optimizer: Small constant for numerical
                        stability. (default: 1e-08)
  --checkpoint_file CHECKPOINT_FILE
                        Set the checkpoint file path and name used for
                        checkpointing the model on disk. This path will only
                        be used to paste the string into the generated code.
                        (default: './')
  --mbsize MBSIZE       The mini-batch size determines how many examples are
                        sampled from the dataset to compute the model error
                        and the updates applied to the model parameters in
                        every iteration of the training.It is common practice
                        to choose "powers of two", e.g. 32, 64, 128, 256.
                        (default: 128)
  --num_epochs NUM_EPOCHS
                        The maximum number of epochs (one epoch = one
                        iteration through the whole data set) for model
                        training. (default: 100)

```

### Model Inference
``test_project/inference.py`` contains the code for applying the trained model to input data for prediction. The inference script was generated for input data that has the following specifications:

#### Input data

```mermaid
graph BT;
    HDF5File/source_file_images-->|"['images'][:]"|InputData/images;
```


Additional inference options are exposed by a commandline interface. You can get an overview of these options by calling the script with the flag ``-h`` or ``--help``:

```bash
python inference.py -h
usage: inference.py [-h] [--source_file_1 SOURCE_FILE_1]
                    [--checkpoint_file CHECKPOINT_FILE] [--clear_devices]
                    [--savepath SAVEPATH]

Command-line interface for applying the trained model on new data.

optional arguments:
  -h, --help            show this help message and exit
  --source_file_1 SOURCE_FILE_1
                        The source file which contains the data for the
                        sources: `images`. (default: './yourpath.hdf5')
  --checkpoint_file CHECKPOINT_FILE
                        The path to the checkpoint file the model is restored
                        from. (default: './')
  --clear_devices       Set to True if the graph device placements should be
                        reseted. (default: False)
  --savepath SAVEPATH   Destination where the predictions are stored. By
                        default, the predictions are simply printed to STDOUT.
                        (default: 'None')

```
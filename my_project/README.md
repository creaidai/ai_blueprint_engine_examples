# my_project

## Project overview

Your project description.

## Setup

### Dependencies

* numpy
* pandas
* tensorflow==1.5

### Project Setup

For running the code and/or continuing development, you need to add the project to your ``PYTHONPATH`` in order for Python to find module imports inside the project directory.

#### Linux / MacOS
We assume in the following that the project is located at ``/home/user/projects``. Please substitute this path by the actual path of your project directory.
The project path can be assigned to ``PYTHONPATH`` for the duration of executing the subsequent command.

```bash
$ PYTHONPATH=/home/user/projects/my_project python training.py
```

We recommend to set the ``PYTHONPATH`` permanently. In case you run Python from within an editor, you can typically set the ``PYTHONPATH`` in the editor configuration. Please consult the documentation of your editor. If you are executing Python from the shell, open the ``$HOME/.bashrc`` file with your favorite editor and add the following line at the bottom of the file:

```bash
$ export PYTHONPATH=$PYTHONPATH:/home/user/projects/my_project
```
To make the changes effective, open a new shell or run the following command in the current shell:

```bash
$ source $HOME/.bashrc
```

#### Windows
On Windows 7, environment variables can be set at ``My Computer > Properties > Advanced System Settings > Environment Variables``. Search under ``System variables`` for ``PYTHONPATH``. If this entry exists, click ``Edit...`` and append the path of the project. If this entry does not exist, click ``New...``, set ``Variable name`` to ``PYTHONPATH`` and ``Variable value`` to the path of the project.

On Windows 10, the environment variables can be se at ``Start > Settings > System > About > System info > Advanced system settings > Environment variables``. Search under ``System variables`` for ``PYTHONPATH``. If this entry exists, click ``Edit...`` and append the path of the project. If this entry does not exist, click ``New...``, set ``Variable name`` to ``PYTHONPATH`` and ``Variable value`` to the path of the project.

If you run Python from within an editor, please consult its documentation to find out how to set the ``PYTHONPATH`` environment variable.

#### Docker

Docker images to run the generated code on both CPU and GPU can be built using the provided Dockerfiles. In order to build a Docker image with CPU-only support, execute the following command:

```bash
$ docker build -t my_project:latest .
```

Similarly, a Docker image with GPU support can be built by executing the following command:

```bash
$ docker build -t my_project:latest -f Dockerfile.gpu .
```

In order to run, e.g., the training script inside a Docker container, data files need to be made available inside the container using [Docker volumes](https://docs.docker.com/storage/volumes/#choose-the--v-or---mount-flag). Assuming `/path/to/your/data` is the directory where training data is stored, `/path/inside/container` is the path where this directory is mounted inside the container, and there is the name of the input source variable is `source_file_1`, then the training script can be executed inside a Docker container as follows:

```bash
$ docker run --rm -v /path/to/data:/path/in/container \
        my_project:latest \
            python my_project/training.py \
                --source_file_1 /path/inside/container/data.csv
```

## Generated Code Overview
In the following a description of the generated code and its basic content is provided. For some of the documentation the Markdown [mermaid](https://mermaidjs.github.io) extension is required to visualize graphs. Alternatively, you can use the [mermaid live editor](https://mermaidjs.github.io/mermaid-live-editor).

### Blueprint

#### Model summary
* A 1 layer neural network that predicts 1 target source from 2 input sources.

#### Model inputs
* `x1` : Float Vector (n x 5)
* `x2` : Float Vector (n x 10)

#### Model targets
* `y` : Binary Class (n)

#### Data Preprocessing
```mermaid
graph BT;
    InputData/x1-->|n x 5|NNInput/input1;
    style NNInput/input1 fill:#d5dbdb, stroke:#626262;
    InputData/x2-->|n x 10|NNInput/input2;
    style NNInput/input2 fill:#d5dbdb, stroke:#626262;
    TargetData/y-->|n|NNTarget/loss_on_y;
    style NNTarget/loss_on_y fill:#abb2b9, stroke:#626262;
```

#### Model Architecture
```mermaid
graph BT;
    NNInput/input1-->|n x 5|Concatenate/conc;
    NNInput/input2-->|n x 10|Concatenate/conc;
    style Concatenate/conc fill:#edbb99, stroke:#626262;
    Concatenate/conc-->|n x 15|DenseLayer/logits;
    style DenseLayer/logits fill:#a9cce3, stroke:#626262;
    DenseLayer/logits-->|n x 1|SigmoidActivation/output;
    style SigmoidActivation/output fill:#d2b4de, stroke:#626262;
```


### Model Training

``test_project/training.py`` contains the code for training the model. The training script was generated for input and target data that have the following specifications:

#### Input data
```mermaid
graph BT;
    CSVFile/source_file_x1-->|"[:, :5]"|InputData/x1;
    CSVFile/source_file_x2-->|"[:, 5:15]"|InputData/x2;
```

#### Target data
```mermaid
graph BT;
    CSVFile/source_file_y-->|"[:, -1]"|TargetData/y;
```


Additional training options are exposed via a commandline interface. You can get an overview of these options by calling the script with the flag ``-h`` or ``--help``:

```bash
python training.py -h
usage: training.py [-h] [--source_file_1 SOURCE_FILE_1]
                   [--learning_rate LEARNING_RATE]
                   [--checkpoint_file CHECKPOINT_FILE] [--mbsize MBSIZE]
                   [--num_epochs NUM_EPOCHS]

Command-line interface for configuring and training the generated model

optional arguments:
  -h, --help            show this help message and exit
  --source_file_1 SOURCE_FILE_1
                        The source file which contains the data for the
                        sources: `x1`, `x2`, `y`. (default: './data.csv')
  --learning_rate LEARNING_RATE
                        Plain SGD optimizer: Learning rate. (default: 0.01)
  --checkpoint_file CHECKPOINT_FILE
                        Set the checkpoint file path and name used for
                        checkpointing the model on disk. This path will only
                        be used to paste the string into the generated code.
                        (default: './')
  --mbsize MBSIZE       The mini-batch size determines how many examples are
                        sampled from the dataset to compute the model error
                        and the updates applied to the model parameters in
                        every iteration of the training.It is common practice
                        to choose "powers of two", e.g. 32, 64, 128, 256.
                        (default: 1000)
  --num_epochs NUM_EPOCHS
                        The maximum number of epochs (one epoch = one
                        iteration through the whole data set) for model
                        training. (default: 10)

```

### Model Inference
``test_project/inference.py`` contains the code for applying the trained model to input data for prediction. The inference script was generated for input data that has the following specifications:

#### Input data

```mermaid
graph BT;
    CSVFile/source_file_x1-->|"[:, :5]"|InputData/x1;
    CSVFile/source_file_x2-->|"[:, 5:15]"|InputData/x2;
```


Additional inference options are exposed by a commandline interface. You can get an overview of these options by calling the script with the flag ``-h`` or ``--help``:

```bash
python inference.py -h
usage: inference.py [-h] [--source_file_1 SOURCE_FILE_1]
                    [--checkpoint_file CHECKPOINT_FILE] [--clear_devices]
                    [--savepath SAVEPATH]

Command-line interface for applying the trained model on new data.

optional arguments:
  -h, --help            show this help message and exit
  --source_file_1 SOURCE_FILE_1
                        The source file which contains the data for the
                        sources: `x2`, `x1`. (default: './data.csv')
  --checkpoint_file CHECKPOINT_FILE
                        The path to the checkpoint file the model is restored
                        from. (default: './')
  --clear_devices       Set to True if the graph device placements should be
                        reseted. (default: False)
  --savepath SAVEPATH   Destination where the predictions are stored. By
                        default, the predictions are simply printed to STDOUT.
                        (default: 'None')

```